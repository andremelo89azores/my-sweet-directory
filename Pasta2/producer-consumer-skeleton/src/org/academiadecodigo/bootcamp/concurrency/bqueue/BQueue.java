package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Blocking Queue
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    int limit;
    Queue<T> queue = new LinkedList<T>();
    /**
     * Constructs a new queue with a maximum size
     * @param limit the queue size
     */
    public BQueue(int limit) {

        this.limit = limit;

        System.out.println(limit);

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     * @param data the data to add to the queue
     */
    public synchronized void offer(T data) {

        while(queue.size() <= limit) {
            try {
                wait();

            }catch (InterruptedException e){
                e.printStackTrace();
            }

            queue.add(data);
            notifyAll();
            System.out.println("sometinhg was added!");
        }

    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     */
    public synchronized T poll() {

        while(queue.size() == 0){

                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }
        T extract = queue.poll();
        notifyAll();
        System.out.println("someting was taken!");
        return extract;

    }

    /**
     * Gets the number of elements in the queue
     * @return the number of elements
     */
    public int getSize() {

        return queue.size();

    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     * @return the maximum number of elements
     */
    public int getLimit() {

        return limit;

    }

}
